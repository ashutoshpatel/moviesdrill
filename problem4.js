// declare the sortMovies function
function sortMovies(obj){

    // if the object does not have any key means the object is empty
    if(Object.keys(obj).length === 0){
        // so return the null
        return null;
    }

    // create array of keys
    const keysArray = Object.keys(obj);

    // use sort method
    const result = keysArray.sort( (a,b) => {
        // get the rating for both movies
        const IMDBRatingOfFirstMovie = obj[a].imdbRating;
        const IMDBRatingOfSecondMovie = obj[b].imdbRating;
        
        // check the which imdb rating is bigger and which is smaller
        const answer = IMDBRatingOfSecondMovie - IMDBRatingOfFirstMovie;

        // if the answer is not 0 means we get bigger rating movie
        if(answer !== 0){
            // so return bigger rating movie
            return answer;
        }

        // if movie rating are same
        // get the earning of both movie
        const earningOfFirstMovie = Number(obj[a].totalEarnings.slice(1,-1));
        const earningOfSecondMovie = Number(obj[b].totalEarnings.slice(1,-1));

        return earningOfSecondMovie - earningOfFirstMovie;
    }).reduce( (acc, key) => {
        // add movie in acc
        acc[key] = obj[key];
        // return the acc object to next step
        return acc;
    }, {});

    // return the result
    return result;
}

// export the sortMovies function
module.exports = sortMovies;