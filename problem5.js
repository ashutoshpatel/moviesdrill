// declare the groupMoviesBasedOnGenre function
function groupMoviesBasedOnGenre(obj){

    // if the object does not have any key means the object is empty
    if(Object.keys(obj).length === 0){
        // so return the null
        return null;
    }

    // create array of keys
    const keysArray = Object.keys(obj);

    // use reduce method
    const result = keysArray.reduce( (acc, key) => {
        // get the current object genre
        const genreArray = obj[key].genre; 

        // iterate on genreArray
        for(let index=0; index<genreArray.length; index++){
            // check the current genreArray[index] genre is present in acc object
            if(acc.hasOwnProperty(genreArray[index])){
                // if yes just add new values in it
                acc[genreArray[index]].push(key);
            }
            else{
                // means the genre is not present so create new array and add it
                acc[genreArray[index]] = [key];
            }
        }

        // return the acc object to next step
        return acc;
    }, {});

    // return the result
    return result;
}

// export the groupMoviesBasedOnGenre function
module.exports = groupMoviesBasedOnGenre;