// import the findAllMoviesWhichGet3OscarNominationsAndEarningMoreThan500M function
const findAllMoviesWhichGet3OscarNominationsAndEarningMoreThan500M = require('../problem2.js');
// import the favouritesMovies object
const favouritesMovies = require('../favouritesMoviesData.js');

// call the findAllMoviesWhichGet3OscarNominationsAndEarningMoreThan500M function
const result = findAllMoviesWhichGet3OscarNominationsAndEarningMoreThan500M(favouritesMovies);

if(result !== null){
    // print the output
    console.log(result);
}
else{
    // means the object is empty
    console.log("Object is empty");
}