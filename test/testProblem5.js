// import the groupMoviesBasedOnGenre function
const groupMoviesBasedOnGenre = require('../problem5.js');
// import the favouritesMovies object
const favouritesMovies = require('../favouritesMoviesData.js');

// call the groupMoviesBasedOnGenre function
const result = groupMoviesBasedOnGenre(favouritesMovies);

if(result !== null){
    // print the output
    console.log(result);
}
else{
    // means the object is empty
    console.log("Object is empty");
}