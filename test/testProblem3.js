// import the getMovieOfActorLeonardoDicaprio function
const getMovieOfActorLeonardoDicaprio = require('../problem3.js');
// import the favouritesMovies object
const favouritesMovies = require('../favouritesMoviesData.js');

// call the getMovieOfActorLeonardoDicaprio function
const result = getMovieOfActorLeonardoDicaprio(favouritesMovies);

if(result !== null){
    // print the output
    console.log(result);
}
else{
    // means the object is empty
    console.log("Object is empty");
}