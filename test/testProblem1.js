// import the findAllMoviesWhichEarningMoreThan500M function
const findAllMoviesWhichEarningMoreThan500M = require('../problem1.js');
// import the favouritesMovies object
const favouritesMovies = require('../favouritesMoviesData.js');

// call the findAllMoviesWhichEarningMoreThan500M function
const result = findAllMoviesWhichEarningMoreThan500M(favouritesMovies);

if(result !== null){
    // print the output
    console.log(result);
}
else{
    // means the object is empty
    console.log("Object is empty");
}