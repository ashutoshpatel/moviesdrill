// import the sortMovies function
const sortMovies = require('../problem4.js');
// import the favouritesMovies object
const favouritesMovies = require('../favouritesMoviesData.js');

// call the sortMovies function
const result = sortMovies(favouritesMovies);

if(result !== null){
    // print the output
    console.log(result);
}
else{
    // means the object is empty
    console.log("Object is empty");
}