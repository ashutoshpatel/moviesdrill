// declare the getMovieOfActorLeonardoDicaprio function
function getMovieOfActorLeonardoDicaprio(obj){

    // if the object does not have any key means the object is empty
    if(Object.keys(obj).length === 0){
        // so return the null
        return null;
    }

    // create array of keys
    const keysArray = Object.keys(obj);

    // use reduce method
    const result = keysArray.reduce( (acc, key) => {
        // get the actors of current object and convert into string
        const currentActors = obj[key].actors.toString();

        // check the current movie actors is Leonardo Dicaprio or not
        if(currentActors.includes("Leonardo Dicaprio")){
            // if yes then add that movie in acc
            acc[key] = obj[key];
        }

        // return the acc object to next step
        return acc;
    }, {});

    // return the result
    return result;
}

// export the getMovieOfActorLeonardoDicaprio function
module.exports = getMovieOfActorLeonardoDicaprio;