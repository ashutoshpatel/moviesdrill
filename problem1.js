// declare the findAllMoviesWhichEarningMoreThan500M function
function findAllMoviesWhichEarningMoreThan500M(obj){

    // if the object does not have any key means the object is empty
    if(Object.keys(obj).length === 0){
        // so return the null
        return null;
    }

    // create array of keys
    const keysArray = Object.keys(obj);

    // use reduce method
    const result = keysArray.reduce( (acc, key) => {
        // get the current movie earning
        const movieEarning = Number(obj[key].totalEarnings.slice(1,-1));

        // check the current movie earning is greater or equal to $500M
        if(movieEarning >= 500){
            // if yes then add that movie in acc
            acc[key] = obj[key];
        }

        // return the acc object to next step
        return acc;
    }, {});

    // return the result
    return result;
}

// export the findAllMoviesWhichEarningMoreThan500M function
module.exports = findAllMoviesWhichEarningMoreThan500M;